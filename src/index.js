const TwitterStream = require('./domain/twitterStream')
//const EventProcessors = require('./domain/event/eventProcessors')
const health = require('@cloudnative/health-connect');

var express = require('express');
var app = express();

app.get('/', function (req, res) {
    res.send('Hello World!');
});

app.listen(3000);

let healthCheck = new health.HealthChecker();


const livePromise = () => new Promise((resolve, _reject) => {
    const appFunctioning = true;
    // You should change the above to a task to determine if your app is functioning correctly
    if (appFunctioning) {
        resolve();
    } else {
        reject(new Error("App is not functioning correctly"));
    }
})

let readyCheck = new health.PingCheck("example.com");
healthCheck.registerReadinessCheck(readyCheck);

app.use('/live', health.LivenessEndpoint(healthCheck));
app.use('/ready', health.ReadinessEndpoint(healthCheck));
app.use('/health', health.HealthEndpoint(healthCheck));


const teste = {
    consumer_key: process.env.consumer_key,
    consumer_secret: process.env.consumer_secret,
    access_token_key: process.env.access_token,
    access_token_secret: process.env.access_token_secret
}

console.log('Blaaaaa:', JSON.stringify(teste))

const twitterStream = new TwitterStream(teste);
//const eventProcessors = new EventProcessors();

twitterStream.start()
//eventProcessors.start()


const eventEmitter = require('./domain/eventEmitter');

setTimeout(function () {
    eventEmitter.emit('stream-config', { track: 'vacina, bolsonaro, trump, biden' });
    eventEmitter.emit('stream-resume');
}, 10000);

