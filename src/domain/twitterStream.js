const eventEmitter = require('./eventEmitter');

/* API Twitter */
//const credentials = require('../twitter.credentials.json');
const Twitter = require('twitter');
const MessageProducer = require('../drivers/messageProducer')

function TwitterStream(conf) {
    if (!(this instanceof TwitterStream)) {
        return new TwitterStream(conf)
    }
    console.log('init ',conf)
    this.eventEmitter = eventEmitter;

    this.client = new Twitter(conf);
    this.messageProducer = new MessageProducer('test-events')

    configureStreamProcessor()

}


function configureStreamProcessor() {

}

TwitterStream.prototype.configure = function (config) {
    //'vacina, bolsonaro, stf'
    this.stream = this.client.stream('statuses/filter', { track: config.track });
}

TwitterStream.prototype.resume = function () {
    if (!this.stream) return

    const that = this;
    this.stream.on('data', function (event) {
        console.log('Dados recebidos: ',event.text)
        that.messageProducer.sendMessage(JSON.stringify(event))
    });
    this.stream.on('error', function (error) {
        console.log('Error received x:', JSON.stringify(error))
        console.log(' Dados: ', JSON.stringify(this.credentials))
    });
    this.stream.on('end', function (event) {
        console.log('Ended received')
    });


}

TwitterStream.prototype.start = function () {
    this.eventEmitter.on('stream-config', (config) => {
        console.log('Event Received, stream-config: ', config);
        this.configure(config)
    });

    this.eventEmitter.on('stream-resume', () => {
        console.log('Event Received, stream-resume: ');
        this.resume()
    });
    this.eventEmitter.on('stream-stop', () => {
        console.log('Event Received, stream-stop: ');
        this.stop()
    });
}

TwitterStream.prototype.stop = function () {
    if (!this.stream) return

    this.stream.destroy();
}



module.exports = TwitterStream
